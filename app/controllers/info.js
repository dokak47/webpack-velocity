var express = require('express'),
  router = express.Router();

module.exports = function (app) {
  app.use('/', router);
};

router.all('/info', function (req, res, next) {
    res.send({msg:'这是使用express控制器'});
    res.end();
});
