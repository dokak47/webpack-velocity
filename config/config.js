var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
    development: {
        root: rootPath,
        app: {
            name: 'webpackosc'
        },
        ip:'http://localhost',
        port: process.env.PORT || 3000,
        viewsPath: path.join(rootPath, 'app/views/'),
        staticPath: '/public',
        proxy:'http://192.169.2.53:8083',
        proxyPath:'/api'
    },

    test: {
        root: rootPath,
        app: {
            name: 'webpackosc'
        },
        ip:'http://localhost',
        port: process.env.PORT || 3000,
        viewsPath: path.join(rootPath, 'app/views/'),
        staticPath: '/public'
    },

    production: {
        root: rootPath,
        app: {
            name: 'webpackosc'
        },
        ip:'http://localhost',
        port: process.env.PORT || 3000,
        viewsPath: path.join(rootPath, 'assets/'),
        staticPath: '/assets'
    }

};

module.exports = config[env];
