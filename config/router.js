var express = require('express'),
    glob = require('glob'),
    _ = require('lodash'),
    router = express.Router(),
    path = require('path'),
    fs = require('fs'),
    rootPath = path.normalize(__dirname + '/..'),
    mockData = require('../app/lib/mockData');

module.exports = function (app) {
    app.use('/', router);
};

router.get('/', function (req, res, next) {
    var fileList = _.chain(mockData.getPageConf()).map(function (file,key) {
        return file;
    }).value();
    res.render('default/home', {data:fileList});
});

// router.get('*.htm', function (req, res, next) {
//     var vmPath = path.dirname(req.url).replace(/^\//,'');
//     var fileName = path.basename(req.url, '.htm');
//     var file_exists = fs.existsSync(path.join(viewsPath,vmPath,fileName + '.vm'));
//     if(file_exists){
//         res.render(path.join(vmPath,fileName), {});
//     }else{
//         res.redirect('/');
//     }
// });
