/*
 * @Author: dmyang
 * @Date:   2015-06-16 15:19:59
 * @Last Modified by:   dmyang
 * @Last Modified time: 2016-03-28 10:27:11
 */

'use strict';

var gulp = require('gulp');
var webpack = require('webpack');
var livereload = require('gulp-livereload');
var nodemon    = require('gulp-nodemon');

var gutil = require('gulp-util');

var webpackConf = require('./webpack.config');
// var webpackDevConf = require('./webpack-dev.config')

var src = process.cwd() + '/public';
var vmSrc = process.cwd() + '/app/views';
var assets = process.cwd() + '/assets';

// js check
gulp.task('hint', function () {
    var jshint = require('gulp-jshint');
    var stylish = require('jshint-stylish');

    return gulp.src([
        '!' + src + '/js/lib/**/*.js',
        src + '/js/**/*.js'
    ])
    // .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

// clean assets
gulp.task('clean', ['hint'], function () {
    var clean = require('gulp-clean');
    return gulp.src(assets, {read: true}).pipe(clean());
});
gulp.task('copy', ['clean'], function () {
    var gulpCopy = require('gulp-file-copy');
    var layouts = vmSrc + '/front/';
    gulp.src(layouts)
        .pipe(gulpCopy(assets, {
            start: layouts
        }))
        .pipe(gulp.dest(assets));
    return gulp
        .src(layouts + '**/*.*')
        .pipe(gulpCopy(assets + '/front', {
            start: layouts
        }))
        .pipe(gulp.dest(assets));
    ;
});

// run webpack pack
gulp.task('pack', ['copy'], function (done) {
    webpack(webpackConf, function (err, stats) {
        if (err) throw new gutil.PluginError('webpack', err);
        gutil.log('[webpack]', stats.toString({colors: true}));
        done();
    });
});

// html process
gulp.task('default', ['pack'], function () {
    // var replace = require('gulp-replace');
    // var htmlmin = require('gulp-htmlmin');


    return gulp
    // .src(assets + '/*.vm')
    // // @see https://github.com/kangax/html-minifier
    // .pipe(htmlmin({
    //     collapseWhitespace: true,
    //     removeComments: true
    // }))
    // .pipe(gulp.dest(assets));
});

// deploy assets to remote server
gulp.task('deploy', function () {
    var sftp = require('gulp-sftp');

    return gulp.src(assets + '/**')
        .pipe(sftp({
            host: '[remote server ip]',
            remotePath: '/www/app/',
            user: 'foo',
            pass: 'bar'
        }));
});

// 使用 nodemone 跑起服务器
gulp.task('server', function (cb) {
    return nodemon({
        script: 'app.js',
        ignore: [
            "tmp/**",
            "public/**",
        ],
        env    : {
            "NODE_ENV": "development"
        }
    });
});
// 当客户端被监听的文件改变时，刷新浏览器
gulp.task('livereload', function() {
    livereload.listen();
    return gulp.watch([
        'app/views/default/**/*.vm',
        'app/views/front/**/*.vm',
        'app/views/front/**/**/*.vm'
    ],{
        readDelay:500
    }, function(event) {
        setTimeout(function(){
            livereload.changed(event.path);
        },500);

    });
});

// serve 任务， 同时开启 serve、livereload 任务
gulp.task('serve', ['server', 'livereload']);